package com.spawpaw.mybatis.generator.gui;

import com.spawpaw.mybatis.generator.gui.controller.BaseController;
import com.spawpaw.mybatis.generator.gui.util.Constants;

import java.util.Locale;

/**
 * 
 * 整个程序的入口
 *
 * @author fbf  server@max256.com
 */
public class GeneratorGuiRunner {
    public static void main(String[] args) {
    	Constants.setLocale(Locale.getDefault());
    	BaseController.launchWindow(args);
//			set your language(only supports CHINESE or ENGLISH)
//        Constants.setLocale(Locale.CHINA);
//        Constants.setLocale(Locale.ENGLISH);
    }
}
