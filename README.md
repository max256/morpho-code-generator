# morpho代码生成器


<!-- Badges section here. -->





<!-- /Badges section end. -->

## 简介

###morpho代码生成器基于mybatis-generator-gui-extension项目独立开发<font color=red>不保证与原项目的兼容性</font>
- 本代码生成器完全兼容morpho脚手架项目
- 95%兼容使用了通用mapper的项目（ui部分和morpho绑定，因此ui部分是这5%您需要手动编写适合您的ui模板）
- 90%兼容mybatis项目（可扩展，修改为合适您的模板代码即可）
- 本项目与mybatis-generator-gui-extension的不同：<font color=red>默认支持JPA，更完善的模板，支持更多的模板里用到的变量像字段注释等，依赖升级</font>
- max256会持续维护该项目作为morpho脚手架推荐代码生成器,由于有一些只适合morpho周边生态的代码特性特此独立出来开发 有可以合并到mybatis-generator-gui-extension的特性会PR到原版本但本独立版本 不保证与原项目的兼容性
- <font color=red>支持mysql5.5-5.7 oracle11g 12c sqlserver postgresql</font>


![界面1](https://images.gitee.com/uploads/images/2018/0829/174003_920c34f4_61523.jpeg "1.jpg")
![界面2](https://images.gitee.com/uploads/images/2018/0829/174019_7983cb37_61523.jpeg "2.jpg")





## 原始功能清单
- `文件合并`：在重新生成代码时，将保留没有添加`@mbg.generated`注释的字段/方法/SQL语句
    - 可合并的文件：`实体类`、`Example`、`Mapper.java`、`Mapper.xml`
- `自动包裹关键字`：对于表名/字段名为关键字的情况，可指定用 ` 或其他字符串包裹，防止冲突
- `实体类增强`
    - `rootClass`(可选)：可为实体对象指定RootClass
    - `trim`(可选)：为String类型的setter添加`trim`
    - `使用真实列名`(可选)：可指定是否使用真实列名
    - `流式构建`(可选)：直接使用`User user= new User().withUsername("uname").withPassword("123");`创建对象
    - `toString`(默认,可取消)：生成toString方法(可选择是否调用父类的`toString`方法)
    - `equals/hashCode` (默认,可取消)：生成`equals/hashCode` 方法（可选择是否调用父类的equals/hashCode 方法）
    - `Serializable`（默认,可取消）：继承`Serializable`接口
- `Example增强`
    - `CaseInsensitiveLike`：添加CaseInsensitiveLike查询方式
    - `ExampleCriteria增强插件`
    - `真实字段获取插件`
    - `分页插件`：提供mysql的基于limit/offset的分页。oracle不提供，请使用pagehelper插件
- `Mapper增强`
    - `指定Mapper类型`：可选择`纯xml`/`java和xml混合`/`纯java`的方式生成mapper
    - `指定主键`：可指定表的主键，将会自动为该表生成获取自增主键的SQL
    - `虚拟主键`：
    - `逻辑删除`
    - `批量插入`
    - `添加@Mapper注解`
    - `各个方法的开关`
- `生成JPA注解`（默认,可取消）
- `指定类头注释`：自定义生成的文件的注释
- `缓存`：采用MBG官方的缓存插件，为生成的查询方法提供cache标签
- 指定BasPackage
- `对象重命名`：
    - `Example` 类的重命名
    - `实体` 类的重命名
    - `Mapper` 类的重命名
- `指定后缀`： 可修改如下项目的后缀：`dao包`、`实体包`、`Mapper`、`Example`、`实体`。
## 本项目独立后增加的功能清单
- `完整的JPA注解`：@Entity @Column @Transient @Id @Table
- `模板变量更丰富`：增加了访问原始字段名 字段类型 字段注释 字段长度 字段是否为空等等变量 方便您编写您的模板
- `morpho脚手架集成`：内置模板完美支持morpho脚手架,自动生成 权限sql entity dao service controller jsp页面 加快项目开发速度减少出错
- `对通用Mapper提供支持`：mapper用户也可以参考使用 其中生成的entity实体完全符合通用Mapper要求的标准[ https://gitee.com/free/Mapper](https://gitee.com/free/Mapper "Mapper")
- `升级依赖到最新稳定版`：max256将持续维护该项目

## 如何使用

> 运行前请确保您的JDK版本为**1.8u40**以上  
> 推荐使用git克隆仓库到本地，这样当本项目更新时直接pull即可，或者直接下载dist目录下的文件即可双机运行jar即可data目录为配置文件

### 方法一：下载源代码

1. 点击右上角**Clone or download**，或用`git命令`拷贝代码仓库：`git clone https://gitee.com/max256/morpho-code-generator.git`

2. 用IDE将源代码导入为Maven项目，然后直接运行`com.spawpaw.mybatis.generator.gui.GeneratorGuiRunner`即可启动


### 方法二：下载jar包

可以下载源码后执行`mvn: package` 自助构建  
或者[选择版本进行下载](https://gitee.com/max256/morpho-code-generator/releases)
dist目录中的是本次发行版的二进制版本直接双击运行即可  

## 二次开发 && 贡献 && 交流 ([进入帮助页面](https://gitee.com/max256/morpho-code-generator/wikis))

- 如果您开发了自己的MBG插件，只需几行代码便可将其暴露到图形界面中，无需了解整个项目的构造。（参见[四步将Plugin的配置暴露到图形化界面中](./wiki/IntegrationOfYourPlugin.md)）


如果您在使用过程中遇到了BUG，或者想让软件添加某些功能，请挂issue或者联系作者：<server@max256.com>

项目地址 https://gitee.com/max256/morpho-code-generator
QQ交流群：210722852

## 其他
如果您觉得本软件对您有帮助，请别忘记给这个项目一个`star`★


